# API REST [EXPRESS](https://expressjs.com/) con [TYPEORM](https://typeorm.io)

se crea un proyecto node
```sh
npm init -y
```

se instala express
```sh
npm install express
```

se crea el archivo <code>src/app.js</code> (este archivo es un simple hello world) y se lo ejecuta con
```sh
node src/app.js
```

se instala typescript
```sh
npm install typescript -D
npm install @types/express --D
```

en nuestro package.json agregamos
```json
    "tsc": "tsc"
```

ahora podeemos hacer
```sh
npm run tsc -- --init
```

eso genera un archivo de configuracion <code>tsconfig.json</code>

ejecutar con

```sh
npm run dev
```