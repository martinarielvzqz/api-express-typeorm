const express = require('express')
const app = express()
const port = 3000

import indexRouter from './routes/index.routes';
import tagRouter from './routes/tag.routes';

app.use(indexRouter);
app.use(tagRouter);


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})