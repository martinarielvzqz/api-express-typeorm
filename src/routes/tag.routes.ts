import { Router } from 'express';
const router = Router();

import { getTags, getTag, createTag, updateTag, deleteTag } from '../controllers/tag.controller';

router.get('/tags', getTags);
router.get('/tags/:id', getTag);
router.post('/tags', createTag);
router.put('/tags/:id', updateTag);
router.delete('/tags/:id', deleteTag);

export default router;