import { Request, Response } from "express";

export const getTags = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return res.json({ mge: 'list' });
};

export const getTag = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return res.json({ mge: 'detail' });
};

export const createTag = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return res.json({ mge: 'create' });
};

export const updateTag = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return res.json({ mge: 'update' });
};

export const deleteTag = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return res.json({ mge: 'delete' });
};


