"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var app = express();
var port = 3000;
var index_routes_1 = __importDefault(require("./routes/index.routes"));
var tag_routes_1 = __importDefault(require("./routes/tag.routes"));
app.use(index_routes_1.default);
app.use(tag_routes_1.default);
app.listen(port, function () {
    console.log("Example app listening at http://localhost:" + port);
});
